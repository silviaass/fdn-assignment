  const popularGroups = [
    { 
        id: 1,
        name: 'Embrace The Curl',
        image: '/image/groups/group1.png',
        description: 'May our curls pop and never stop!'
    },
    { 
        id: 2,
        name: 'Embrace The Curl',
        image: '/image/groups/group2.png',
        description: 'May our curls pop and never stop!'
    },
    { 
        id: 3,
        name: 'Embrace The Curl',
        image: '/image/groups/group3.png',
        description: 'May our curls pop and never stop!'
    },
    { 
        id: 4,
        name: 'Embrace The Curl',
        image: '/image/groups/group4.png',
        description: 'May our curls pop and never stop!'
    },
  ]
  
  export function getGroups() {
    return popularGroups
  }
  