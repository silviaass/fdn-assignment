export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'FDN Front-End Assignment',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'FDN Assignment for Front End' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/image/favicon.png' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/icon?family=Material+Icons|Material+Icons+Outlined|' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700&display=swap'},
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;600;700&display=swap'},
      { rel: 'stylesheet', href: '/css/materialize.min.css' },
    ]
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {
    proxy: true
  },
  proxy: {
    '/api/': {
      target: process.env.API_BASE_URL,
      pathRewrite: {
        '^/api' : '/'
      }
    },
  },
  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
  },
  env: {
    baseUrl: process.env.BASE_URL,
    app: {
      mode: process.env.APP_MODE
    }
  }
}
