# FDN Front-End Assignment

## Build Setup

```bash
# Install dependencies
$ npm install
$ npm install --save vue-slick-carousel

# Define environment variable
$ Copy file from .env.example then make a file named ".env"

# Serve with hot reload at localhost:3000
$ npm run dev

```
## Additional Information

```bash
# Dataset
$ Dataset from API is not complete yet. I decided to add static dataset for each section, located at /api/static

# Image
$ Static images are located at static/image

# Window size 
$ Window width for web desktop only, as attached in required design at width: 1440px

$ This web app is going live here: http://fdn-assignment.herokuapp.com/

```