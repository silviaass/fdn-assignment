  const topBrands = [
    { 
        id: 1,
        image : '/image/brands/nivea.png',
        name : 'Nivea',
    },
    { 
        id: 2,
        image : '/image/brands/the-ordinary.png',
        name : 'The Ordinary',
    },
    { 
        id: 3,
        image : '/image/brands/the-body-shop.png',
        name : 'The Body Shop',
    },
    { 
        id: 4,
        image : '/image/brands/sk-ii.png',
        name : 'SK-II',
    },
    { 
        id: 5,
        image : '/image/brands/maybelline.png',
        name : 'Maybelline',
    },
    { 
        id: 6,
        image : '/image/brands/innisfree.png',
        name : 'Innisfree',
    }
  ]
  
  export function getTopBrands() {
    return topBrands
  }
  