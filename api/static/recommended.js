  const recommended = [
    { 
        id: 1,
        image : '/image/products/peach-blush.png',
        detail: 'Match Skin Type',
        name : 'VAL by Valerie Thomas',
        description : 'Pure Pressed Blush',
        color: 'Neutral Rose',
        rating : 4.9,        
        review_count: 7
    },
    { 
        id: 2,
        image : '/image/products/foundation.png',
        detail: 'Match Skin Type',
        name : 'VAL by Valerie Thomas',
        description : 'Phito Pigment Liquid Foundation',
        color: 'Neutral Rose',
        rating : 4.9,
        review_count: 7
    },
    { 
        id: 3,
        image : '/image/products/serum.png',
        detail: 'Match Skin Type',
        name : 'VAL by Valerie Thomas',
        description : 'Pure Pressed Blush',
        color: 'Neutral Rose',
        rating : 4.9,
        review_count: 7
    }
  ]
  
  export function getRecommended() {
    return recommended
  }
  