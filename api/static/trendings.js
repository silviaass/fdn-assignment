  const trendings = [
    { 
        id: 1,
        image : 'image/products/serum.png',
        name : 'SkinCeuticals',
        description : 'C E Ferulics',
        color: '',
        rating : 4.9,
        review_count: 7
    },
    { 
        id: 2,
        image : '/image/products/foundation.png',
        name : 'Juice Beauty',
        description : 'Phyto-Pigments Flawless Serum Foundation',
        color: 'Rosy Beige',
        rating : 4.9,
        review_count: 7
    },
    { 
        id: 3,
        image : '/image/products/pink-blush.png',
        name : 'Juice Beauty',
        description : 'Pure Pressed Blush',
        color: 'Neutral Rose',
        rating : 4.9,
        review_count: 7
    },
    { 
        id: 4,
        image : '/image/products/peach-blush.png',
        name : 'VAL by Valerie Thomas',
        description : 'Pure Pressed Blush',    
        color: 'Neutral Rose',    
        rating : 4.9,
        review_count: 7
    },
    { 
        id: 5,
        image : '/image/products/serum.png',
        name : 'SkinCeuticals',
        description : 'C E Ferulics',
        color: '',
        rating : 4.9,
        review_count: 7
    },
    { 
        id: 6,
        image : '/image/products/foundation.png',
        name : 'Juice Beauty',
        description : 'Phyto-Pigments Flawless Serum Foundation',
        color: 'Rosy Beige',
        rating : 4.9,
        review_count: 7
    },
    { 
        id: 7,
        image : '/image/products/pink-blush.png',
        name : 'Juice Beauty',
        description : 'Pure Pressed Blush',
        color: 'Neutral Rose',
        rating : 4.9,
        review_count: 7
    },
    { 
        id: 8,
        image : '/image/products/pink-blush.png',
        name : 'Juice Beauty',
        description : 'Pure Pressed Blush',
        color: 'Neutral Rose',
        rating : 4.9,
        review_count: 7
    },
    { 
        id: 9,
        image : '/image/products/peach-blush.png',
        name : 'VAL by Valerie Thomas',
        description : 'Pure Pressed Blush',    
        color: 'Neutral Rose',    
        rating : 4.9,
        review_count: 7
    },
  ]
  
  export function getTrendings() {
    return trendings
  }
  